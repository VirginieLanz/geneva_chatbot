# -----------------------------------------------------------
# Itinerary segment
# -----------------------------------------------------------
def itinerary(update, context):
    user = update.message.from_user
    user_itinerary = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_itinerary)
    update.message.reply_text("Merci d'envoyer votre localisation (via piece jointe ou simplement en texte)")
    if user_itinerary in ['Swiss']:
        return swiss_restaurant_itinerary(update, context)
    if user_itinerary in ['Italian']:
        return italian_restaurant_itinerary(update, context)
    if user_itinerary in ['Japanese']:
        return japanese_restaurant_itinerary(update, context)
    if user_itinerary in ['Indian']:
        return indian_restaurant_itinerary(update, context)
    if user_itinerary in ['Chinese']:
        return chinese_restaurant_itinerary(update, context)

    return ConversationHandler.END


# -----------------------------------------------------------
# Swiss restaurants
# -----------------------------------------------------------
def swiss_restaurant_itinerary(update, context):
    user = update.message.from_user
    user_swiss_restaurant_itinerary = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_swiss_restaurant_itinerary)
    if user_swiss_restaurant_itinerary in ['Le Gruyerien']:
        return le_gruyierien(update, context)
    if user_swiss_restaurant_itinerary in ['Le National']:
        return le_national(update, context)
    if user_swiss_restaurant_itinerary in ['Edelweiss']:
        return edelweiss(update, context)

    return ConversationHandler.END


def le_gruyierien(update, context):
    reply_keyboard = [['See you soon !']['Return']]

    user = update.message.from_user
    logger.info("The choice of %s is: %s", user.first_name, update.message.text)
    update.message.reply_text('Please send your location (via attachment or simply in text)\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ConversationHandler.END


def le_national(update, context):
    reply_keyboard = [['See you soon !']['Return']]

    user = update.message.from_user
    logger.info("The choice of %s is: %s", user.first_name, update.message.text)
    update.message.reply_text('Please send your location (via attachment or simply in text)\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ConversationHandler.END


def edelweiss(update, context):
    reply_keyboard = [['See you soon !']['Return']]

    user = update.message.from_user
    logger.info("The choice of %s is: %s", user.first_name, update.message.text)
    update.message.reply_text('Please send your location (via attachment or simply in text)\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ConversationHandler.END


# -----------------------------------------------------------
# Italian restaurants
# -----------------------------------------------------------
def italian_restaurant_itinerary(update, context):
    user = update.message.from_user
    user_italian_restaurant_itinerary = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_italian_restaurant_itinerary)
    if user_italian_restaurant_itinerary in ['TOSCA']:
        return tosca(update, context)
    if user_italian_restaurant_itinerary in ['Les Trois Verres']:
        return les_trois_verres(update, context)
    if user_italian_restaurant_itinerary in ['Le Casanova']:
        return le_casanova(update, context)

    return ConversationHandler.END


def tosca(update, context):
    reply_keyboard = [['See you soon !']['Return']]

    user = update.message.from_user
    logger.info("The choice of %s is: %s", user.first_name, update.message.text)
    update.message.reply_text('Please send your location (via attachment or simply in text)\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ConversationHandler.END


def les_trois_verres(update, context):
    reply_keyboard = [['See you soon !']['Return']]

    user = update.message.from_user
    logger.info("The choice of %s is: %s", user.first_name, update.message.text)
    update.message.reply_text('Please send your location (via attachment or simply in text)\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ConversationHandler.END


def le_casanova(update, context):
    reply_keyboard = [['See you soon !']['Return']]

    user = update.message.from_user
    logger.info("The choice of %s is: %s", user.first_name, update.message.text)
    update.message.reply_text('Please send your location (via attachment or simply in text)\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ConversationHandler.END

# -----------------------------------------------------------
# Japanese restaurants
# -----------------------------------------------------------
def japanese_restaurant_itinerary(update, context):
    user = update.message.from_user
    user_swiss_restaurant_itinerary = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_swiss_restaurant_itinerary)
    if user_swiss_restaurant_itinerary in ['TOSCA']:
        return tosca(update, context)
    if user_swiss_restaurant_itinerary in ['Les Trois Verres']:
        return les_trois_verres(update, context)
    if user_swiss_restaurant_itinerary in ['Le Casanova']:
        return le_casanova(update, context)

    return ConversationHandler.END
# -----------------------------------------------------------
# Indian restaurants
# -----------------------------------------------------------
# -----------------------------------------------------------
# Chinese restaurants
# -----------------------------------------------------------
