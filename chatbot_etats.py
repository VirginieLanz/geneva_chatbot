#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Faire la fonction return
# Faire en sorte que l'itinéraire fonctionne ?
import sys, logging, requests, time, math
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, bot, Update)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, ConversationHandler, CallbackContext)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

bot_token = sys.argv[1]
logger = logging.getLogger(__name__)

CHOICE, ACTIVITY_CHOICE, OUTINGS_CHOICE, OUTINGS_RECOMMENDATION, RESTAURANT_CHOICE, RESTAURANT_DETAILS, ITINERARY = range(7)


def start(update, context):
    reply_keyboard = [["Alright !"]]
    update.message.reply_text(
        'Hello there ! 🌞\n'
        'I am a lovely bot hear to help you to find a'
        'great restaurant or other nice things to do in Geneva.\n\n'
        'Send /cancel to stop talking to me.\n\n',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return CHOICE


def choice(update, context):
    reply_keyboard = [['Activity', 'Restaurant']]

    user = update.message.from_user
    logger.info("The choice of %s is: %s", user.first_name, update.message.text)
    update.message.reply_text('You know what ? \nThere is plenty of things to do here in Geneva, hourra ! \n\n'
                              'For example, do you want to eat something or do you want to do something else ? 🍻\n\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ACTIVITY_CHOICE


# -----------------------------------------------------------
# Activities segment
# -----------------------------------------------------------
def activities(update, context):
    reply_keyboard = [['Museums'], ['Bars'], ['Clubs']]

    user = update.message.from_user
    user_activities_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_activities_choice)
    update.message.reply_text('So, do you prefer to go to the museum, have a drink or go to clubs ?\n'
                              'Otherwise, we can also offer you everything at once !\n\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return OUTINGS_CHOICE


def museums(update, context):
    reply_keyboard = [["Thank you !"]]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('Nice idea ! There are a lot of great museums at Genava !\n'
                              'For exemple: \n\n'
                              '– If you like contemporary art there is MAMCO :-) \n\n'
                              "– If you like history, you can go to the Musée d'histoire naturelle de Genève. \n\n"
                              '– And if you prefer the nature, Conservatoire et Jardin '
                              'botaniques de la ville de Genève is for you ! \n\n\n'
                              'Enjoy Geneva !\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return OUTINGS_RECOMMENDATION


def bars(update, context):
    reply_keyboard = [["Thank you !"]]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('Uh, a little drink then ?\n'
                              'For the pubs: \n\n'
                              "– L'Elephant dans la canette with a specific charm ! \n\n"
                              '– Les philosophes with a lovely decoration. \n\n'
                              '– Last but not least, La Ferblanterie for absolutely delicious Gin tonic :-) \n\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return OUTINGS_RECOMMENDATION


def clubs(update, context):
    reply_keyboard = [["Thank you ! "]]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('There is nothing better than dancing !\n'
                              'So, for the clubs: \n\n'
                              '– Audio club for discovering great electro artist ! \n\n'
                              "– L'Usine is a great place if you want to dance in a special place :-) \n\n"
                              "– Just next to L'Usine, there is Le Zoo and it's fabulous too. \n"
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return OUTINGS_RECOMMENDATION


# -----------------------------------------------------------
# Restaurants segment
# -----------------------------------------------------------
def restaurants_type(update, context):
    reply_keyboard = [['Swiss 🥨'], ['Italian 🍝'], ['Japanese 🍜'], ['Indian 🥙'], ['Chinese 🥟']]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('You want to eat something ? Alright ! \n'
                              'We can offer you 5 types of restaurants. Make your choice !\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT_CHOICE


def swiss_restaurant(update, context):
    reply_keyboard = [['Le Gruyerien'], ['Le National'], ['Edelweiss']]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('Ha, delightful swiss dishes ! 🤤\n'
                              'Here you are 3 good restaurants in Geneva just for you: \n\n'
                              '"Le Gruyerien", for the best planchettes in town. \n'
                              'The one and the unique "Le National".\n'
                              'And the third one, "Edelweiss" for all the switzerland lovers ! \n\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT_DETAILS


def italian_restaurant(update, context):
    reply_keyboard = [['TOSCA'], ['Les Trois Verres'], ['Le Casanova']]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text("Che bella idea portare un po 'di Italia nel tuo piatto! \n"
                              'For a beautiful plate full of flavor, we suggest: \n'
                              'The TOSCA, for a moment full of sweets. \n'
                              'Les Trois Verres because we love to drink well too !\n'
                              'Le Casanova, because why not ?\n\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT_DETAILS


def japanese_restaurant(update, context):
    reply_keyboard = [['Izumi'], ['Ukiyo Grenus'], ['Kanikuma']]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('Your attention please, Japan is in the spotlight!\n'
                              '"Izumi" is a really good choice for having a little bit Japan in your day. \n'
                              'Ukiyo Grenus for a genius idea ! \n'
                              'Kanikuma to rule them all.\n\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT_DETAILS


def indian_restaurant(update, context):
    reply_keyboard = [['Chandigarh Tandoori'], ['Kiran'], ['Nirvana']]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('Mirror, oh my beautiful mirror, tell me which is the best Indian '
                              'restaurant in Geneva ?\n'
                              '"Chandigarh Tandoori", probably the creator of the delicious Tandoori Chicken.'
                              'Yeah, probably not. But you know, we all love Tandoori right ?'
                              '"Kiran" you said ? Yeah, we know it already ! Sooo good. \n'
                              'There is a fabulous Indian restaurant named... Nirvana ? Well, no kidding !\n\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT_DETAILS


def chinese_restaurant(update, context):
    reply_keyboard = [['JIAWEI'], ['Tse Fung'], ['Canard Intelligent']]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("The choice of %s is: %s", user.first_name, user_choice)
    update.message.reply_text('HELP, HELP ! WE ARE SEARCHING FOR A GREAT CHINESE RESTAURANT, PLEASE, HELP !\n'
                              "Eh, don't worry. We are here for you. JIAWEI can probably make you happy. \n"
                              "Tse Fung is here for you too. Really, it's so so good !\n"
                              "you know 'Canard Intelligent' ? In english, it's smart duck. "
                              'If a duck got it, you can too !\n\n\n'
                              'If you changed your mind, send /previous.\n'
                              'If you want to access the transport functionality, send /itinerary.\n'
                              'Send /cancel to stop talking to me.\n\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT_DETAILS


def restaurant_details(update, context):
    reply_keyboard = [["Thank you ! "]]

    user = update.message.from_user
    user_choice = update.message.text
    logger.info("Details of %s restaurant", user_choice)
    update.message.reply_text('Here is the location of the restaurant you have wisely chosen. Enjoy your meal !\n',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    # Swiss restaurant
    if user_choice in ['Le Gruyerien']:
        update.message.reply_location(46.194489, 6.209360)

    if user_choice in ['Le National']:
        update.message.reply_location(46.276946, 6.169039)

    if user_choice in ['Edelweiss']:
        update.message.reply_location(46.211508, 6.149985)

    # Italian restaurant
    if user_choice in ['TOSCA']:
        update.message.reply_location(46.194489, 6.209360)

    if user_choice in ['Les Trois Verres']:
        update.message.reply_location(46.276946, 6.169039)

    if user_choice in ['Le Casanova']:
        update.message.reply_location(46.211508, 6.149985)

    # Japanese restaurant
    if user_choice in ['Izumi']:
        update.message.reply_location(46.194489, 6.209360)

    if user_choice in ['Ukiyo Grenus']:
        update.message.reply_location(46.276946, 6.169039)

    if user_choice in ['Kanikuma']:
        update.message.reply_location(46.211508, 6.149985)

    # Indian restaurant
    if user_choice in ['Chandigarh Tandoori']:
        update.message.reply_location(46.194489, 6.209360)

    if user_choice in ['Kiran']:
        update.message.reply_location(46.276946, 6.169039)

    if user_choice in ['Nirvana']:
        update.message.reply_location(46.211508, 6.149985)

    # Chinese restaurant
    if user_choice in ['JIAWEI']:
        update.message.reply_location(46.194489, 6.209360)

    if user_choice in ['Tse Fung']:
        update.message.reply_location(46.276946, 6.169039)

    if user_choice in ['Canard Intelligent']:
        update.message.reply_location(46.211508, 6.149985)

    return RESTAURANT_DETAILS


# -----------------------------------------------------------
# Transport segment
# -----------------------------------------------------------
def call_opendata(path):
    url = "http://transport.opendata.ch/v1/" + path
    response = requests.get(url)
    return response.json()


def calculate_start_time(timestamp):
    seconds = timestamp-time.time()
    minutes = math.floor(seconds/60)
    if minutes < 1:
        return "Oops ! It's time to run !\n"
    if minutes > 60:
        return "> {} h.".format(math.floor(minutes/60))
    return "dans {} min.".format(minutes)


# Messages
def show_stops(update, stops):
    logger.info("Display of stops")
    response_text = "Here are the stops:\n"
    for station in stops['stations']:
        if station['id'] is not None:
            response_text += "\n/a" + station['id'] + " " + station['name']
    update.message.reply_text(response_text)


def show_departures(update, departures):
    response_text = "Here are the next departures:\n\n"
    for departure in departures['stationboard']:
        response_text += "{} {} dest. {} - {}\n".format(
            departure['category'],
            departure['number'],
            departure['to'],
            calculate_start_time(departure['stop']['departureTimestamp'])
        )
    response_text += "\nShow again: /a" + departures['station']['id']

    coordinate = departures['station']['coordinate']
    update.message.reply_location(coordinate['x'], coordinate['y'])
    update.message.reply_text(response_text)


# -----------------------------------------------------------
# Itinerary
# -----------------------------------------------------------
def location(update: Update, context: CallbackContext):
    update.message.reply_text("Please send your location (via attachment or simply in text)")
    logger.info("Itinerary state")

    return ITINERARY


# Different responses
def place_to_look(update: Update, context: CallbackContext):
    results_opendata = call_opendata("locations?query=" + update.message.text)
    logger.info("Opendata research")
    show_stops(update, results_opendata)


def coordinates_to_treat(update: Update, context: CallbackContext):
    location = update.message.location
    logger.info("Coordinate processing")
    results_opendata = call_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
    show_stops(update, results_opendata)


def stop_details(update: Update, context: CallbackContext):
    logger.info("Detailed display of stops")
    stop_id = update.message.text[2:]
    show_departures(update, call_opendata("stationboard?id=" + stop_id))


# -----------------------------------------------------------
# Constants
# -----------------------------------------------------------
def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye then ! See you soon :-)\n',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


# -----------------------------------------------------------
# Main function with states
# -----------------------------------------------------------
def main():
    # Token
    updater = Updater("1110106170:AAFuDgLuKFFJXZFi1Q2V0XU6HXx1NfILSxA", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start), CommandHandler('itinerary', location)],

        states={

            CHOICE: [
                MessageHandler(Filters.regex('^(Alright !)$'), choice),
                CommandHandler('itinerary', location)
            ],

            ACTIVITY_CHOICE: [
                MessageHandler(Filters.regex('^(Restaurant)$'), restaurants_type),
                MessageHandler(Filters.regex('^(Activity)$'), activities),
                CommandHandler('previous', choice),
                CommandHandler('itinerary', location)
            ],

            OUTINGS_CHOICE: [
                MessageHandler(Filters.regex('^(Museums)$'), museums),
                MessageHandler(Filters.regex('^(Bars)$'), bars),
                MessageHandler(Filters.regex('^(Clubs)$'), clubs),
                CommandHandler('itinerary', location),
                CommandHandler('previous', choice)
            ],

            OUTINGS_RECOMMENDATION: [
                MessageHandler(Filters.regex('^(Thank you !)$'), choice),
                CommandHandler('itinerary', location)
            ],

            RESTAURANT_CHOICE: [
                MessageHandler(Filters.regex('^(Swiss 🥨)$'), swiss_restaurant),
                MessageHandler(Filters.regex('^(Italian 🍝)$'), italian_restaurant),
                MessageHandler(Filters.regex('^(Japanese 🍜)$'), japanese_restaurant),
                MessageHandler(Filters.regex('^(Indian 🥙)$'), indian_restaurant),
                MessageHandler(Filters.regex('^(Chinese 🥟)$'), chinese_restaurant),
                CommandHandler('itinerary', location),
                CommandHandler('previous', choice)
            ],

            RESTAURANT_DETAILS: [
                MessageHandler(Filters.regex('^(Le Gruyerien|'
                                             'Le National|'
                                             'Edelweiss|'
                                             'TOSCA|'
                                             'Les Trois Verres|'
                                             'Le Casanova|'
                                             'Izumi|'
                                             'Ukiyo Grenus|'
                                             'Kanikuma|'
                                             'Chandigarh Tandoori|'
                                             'Kiran|'
                                             'Nirvana|'
                                             'JIAWEI|'
                                             'Tse Fung|'
                                             'Canard Intelligent)$'), restaurant_details),
                CommandHandler('itinerary', location),
                MessageHandler(Filters.regex('^(Thank you !)$'), choice),
                CommandHandler('previous', restaurants_type)
            ],

            ITINERARY: [
                MessageHandler(Filters.regex('^(Thank you !)$'), location),
                MessageHandler(Filters.text, place_to_look),
                MessageHandler(Filters.location, coordinates_to_treat),
                MessageHandler(Filters.command, stop_details)
            ]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
